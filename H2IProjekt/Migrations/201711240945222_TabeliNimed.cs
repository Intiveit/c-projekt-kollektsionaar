namespace H2IProjekt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TabeliNimed : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Eses", newName: "Ese");
            RenameTable(name: "dbo.EsemeLiiks", newName: "EsemeLiik");
            RenameTable(name: "dbo.Kasutajas", newName: "Kasutaja");
            RenameTable(name: "dbo.KasutajaKogus", newName: "KasutajaKogu");
            RenameTable(name: "dbo.KuulubKogusses", newName: "KuulubKogusse");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.KuulubKogusse", newName: "KuulubKogusses");
            RenameTable(name: "dbo.KasutajaKogu", newName: "KasutajaKogus");
            RenameTable(name: "dbo.Kasutaja", newName: "Kasutajas");
            RenameTable(name: "dbo.EsemeLiik", newName: "EsemeLiiks");
            RenameTable(name: "dbo.Ese", newName: "Eses");
        }
    }
}
