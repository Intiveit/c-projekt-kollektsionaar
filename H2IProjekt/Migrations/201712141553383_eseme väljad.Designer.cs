// <auto-generated />
namespace H2IProjekt.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class esemeväljad : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(esemeväljad));
        
        string IMigrationMetadata.Id
        {
            get { return "201712141553383_eseme väljad"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
