namespace H2IProjekt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Kasutaja_tabel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Kasutaja", "Eesnimi", c => c.String());
            AddColumn("dbo.Kasutaja", "Perenimi", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Kasutaja", "Perenimi");
            DropColumn("dbo.Kasutaja", "Eesnimi");
        }
    }
}
