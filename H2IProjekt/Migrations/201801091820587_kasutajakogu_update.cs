namespace H2IProjekt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class kasutajakogu_update : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.KasutajaKogu", "KasutajaId", c => c.Int(nullable: false));
            AddColumn("dbo.KasutajaKogu", "KoguId", c => c.Int(nullable: false));
            CreateIndex("dbo.KasutajaKogu", "KasutajaId");
            CreateIndex("dbo.KasutajaKogu", "KoguId");
            AddForeignKey("dbo.KasutajaKogu", "KasutajaId", "dbo.Kasutaja", "KasutajaId", cascadeDelete: true);
            AddForeignKey("dbo.KasutajaKogu", "KoguId", "dbo.Kogu", "KoguId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.KasutajaKogu", "KoguId", "dbo.Kogu");
            DropForeignKey("dbo.KasutajaKogu", "KasutajaId", "dbo.Kasutaja");
            DropIndex("dbo.KasutajaKogu", new[] { "KoguId" });
            DropIndex("dbo.KasutajaKogu", new[] { "KasutajaId" });
            DropColumn("dbo.KasutajaKogu", "KoguId");
            DropColumn("dbo.KasutajaKogu", "KasutajaId");
        }
    }
}
