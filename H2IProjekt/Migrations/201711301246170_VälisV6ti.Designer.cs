// <auto-generated />
namespace H2IProjekt.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class VälisV6ti : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(VälisV6ti));
        
        string IMigrationMetadata.Id
        {
            get { return "201711301246170_VälisV6ti"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
