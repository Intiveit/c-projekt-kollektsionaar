namespace H2IProjekt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EseAnnotatsioon : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ese", "Kogus", c => c.Short(nullable: false));
            AlterColumn("dbo.Ese", "Nimi", c => c.String(nullable: false, maxLength: 80));
            AlterColumn("dbo.Ese", "Autor", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Ese", "Kirjeldus", c => c.String(maxLength: 400));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Ese", "Kirjeldus", c => c.String());
            AlterColumn("dbo.Ese", "Autor", c => c.String());
            AlterColumn("dbo.Ese", "Nimi", c => c.String());
            DropColumn("dbo.Ese", "Kogus");
        }
    }
}
