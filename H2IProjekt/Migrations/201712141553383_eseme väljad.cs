namespace H2IProjekt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class esemeväljad : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ese", "OnOlemas", c => c.Boolean(nullable: false));
            AddColumn("dbo.Ese", "Kirjeldus", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ese", "Kirjeldus");
            DropColumn("dbo.Ese", "OnOlemas");
        }
    }
}
