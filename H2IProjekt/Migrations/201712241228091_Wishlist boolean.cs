namespace H2IProjekt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Wishlistboolean : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ese", "OnWishlistis", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ese", "OnWishlistis");
        }
    }
}
