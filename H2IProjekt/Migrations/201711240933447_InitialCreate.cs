namespace H2IProjekt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Eses",
                c => new
                    {
                        EseId = c.Int(nullable: false, identity: true),
                        Nimi = c.String(),
                        Autor = c.String(),
                        Aasta = c.Short(nullable: false),
                        Hind = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.EseId);
            
            CreateTable(
                "dbo.EsemeLiiks",
                c => new
                    {
                        EsemeLiikId = c.Int(nullable: false, identity: true),
                        Nimi = c.String(),
                    })
                .PrimaryKey(t => t.EsemeLiikId);
            
            CreateTable(
                "dbo.Kasutajas",
                c => new
                    {
                        KasutajaId = c.Int(nullable: false, identity: true),
                        Kasutajanimi = c.String(),
                        Parool = c.String(),
                    })
                .PrimaryKey(t => t.KasutajaId);
            
            CreateTable(
                "dbo.KasutajaKogus",
                c => new
                    {
                        KasutajaKoguId = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.KasutajaKoguId);
            
            CreateTable(
                "dbo.KuulubKogusses",
                c => new
                    {
                        KuulubKogusseId = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.KuulubKogusseId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.KuulubKogusses");
            DropTable("dbo.KasutajaKogus");
            DropTable("dbo.Kasutajas");
            DropTable("dbo.EsemeLiiks");
            DropTable("dbo.Eses");
        }
    }
}
