namespace H2IProjekt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VälisV6ti : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Kogu",
                c => new
                    {
                        KoguId = c.Int(nullable: false, identity: true),
                        Nimi = c.String(),
                    })
                .PrimaryKey(t => t.KoguId);
            
            AddColumn("dbo.KuulubKogusse", "EseId", c => c.Int(nullable: false));
            AddColumn("dbo.KuulubKogusse", "KoguId", c => c.Int(nullable: false));
            CreateIndex("dbo.KuulubKogusse", "EseId");
            CreateIndex("dbo.KuulubKogusse", "KoguId");
            AddForeignKey("dbo.KuulubKogusse", "EseId", "dbo.Ese", "EseId", cascadeDelete: true);
            AddForeignKey("dbo.KuulubKogusse", "KoguId", "dbo.Kogu", "KoguId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.KuulubKogusse", "KoguId", "dbo.Kogu");
            DropForeignKey("dbo.KuulubKogusse", "EseId", "dbo.Ese");
            DropIndex("dbo.KuulubKogusse", new[] { "KoguId" });
            DropIndex("dbo.KuulubKogusse", new[] { "EseId" });
            DropColumn("dbo.KuulubKogusse", "KoguId");
            DropColumn("dbo.KuulubKogusse", "EseId");
            DropTable("dbo.Kogu");
        }
    }
}
