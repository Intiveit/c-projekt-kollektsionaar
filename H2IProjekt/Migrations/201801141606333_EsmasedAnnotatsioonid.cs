namespace H2IProjekt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EsmasedAnnotatsioonid : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Kogu", "Nimi", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Kasutaja", "Kasutajanimi", c => c.String(nullable: false, maxLength: 25));
            AlterColumn("dbo.Kasutaja", "Parool", c => c.String(nullable: false, maxLength: 80));
            AlterColumn("dbo.Kasutaja", "Eesnimi", c => c.String(nullable: false, maxLength: 80));
            AlterColumn("dbo.Kasutaja", "Perenimi", c => c.String(nullable: false, maxLength: 80));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Kasutaja", "Perenimi", c => c.String());
            AlterColumn("dbo.Kasutaja", "Eesnimi", c => c.String());
            AlterColumn("dbo.Kasutaja", "Parool", c => c.String());
            AlterColumn("dbo.Kasutaja", "Kasutajanimi", c => c.String());
            AlterColumn("dbo.Kogu", "Nimi", c => c.String());
        }
    }
}
