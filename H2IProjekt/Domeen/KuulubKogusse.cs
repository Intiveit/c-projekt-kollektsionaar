﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H2IProjekt.Domeen
{
    public class KuulubKogusse
    {
        [Key]
        public int KuulubKogusseId { get; set; }
        [Required]
        public int EseId { get; set; }
        public virtual Ese Ese { get; set; }
        [Required]
        public int KoguId { get; set; }
        public virtual Kogu Kogu { get; set; }

    }
}
