﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H2IProjekt.Domeen
{
    public class Kasutaja
    {
        [Key]
        public int KasutajaId { get; set; }
        [Required]
        [MaxLength(length: 25)]
        [MinLength(length: 4)]
        public string Kasutajanimi { get; set; }
        [Required]
        [MaxLength(length: 80)]
        [MinLength(length: 4)]
        public string Parool { get; set; }
        [Required]
        [MaxLength(length: 80)]
        public string Eesnimi { get; set; }
        [Required]
        [MaxLength(length: 80)]
        public string Perenimi { get; set; }

    }
}
