﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H2IProjekt.Domeen
{
    public class KasutajaKogu
    {
        [Key]
        public int KasutajaKoguId { get; set; }        
        public int KasutajaId { get; set; }
        public virtual Kasutaja Kasutaja { get; set; }

        public int KoguId { get; set; }
        public virtual Kogu Kogu { get; set; }
    }
}
