﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H2IProjekt.Domeen
{
    public class Ese
    {
        [Key]
        public int EseId { get; set; }
        [Required]
        [MaxLength(length: 80)]
        public string Nimi { get; set; }
        [Required]
        [MaxLength(length: 100)]
        public string Autor { get; set; }
        [Range(-5000,3000)]
        public short Aasta { get; set; }
        [Range(0, 1000000000)]
        public decimal Hind { get; set; }
        public bool OnOlemas { get; set; }
        public bool OnWishlistis { get; set; }
        [MaxLength(length: 400)]
        public string Kirjeldus { get; set; }
        [Range(0, 1000)]
        public short Kogus { get; set; }

        public virtual List<KuulubKogusse> KuulubKogusse { get; set; }
    }
}
