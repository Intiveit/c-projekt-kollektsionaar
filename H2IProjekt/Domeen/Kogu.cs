﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H2IProjekt.Domeen
{
    public class Kogu
    {
        [Key]
        public int KoguId { get; set; }
        [Required]
        [MaxLength(length: 50)]
        public string Nimi { get; set; }

        //Kogusse kuulub mitu eset
        public virtual List<KuulubKogusse> KuulubKogusse { get; set; }

        public override string ToString()
        {
            return $"{Nimi}";
        }
    }
}
