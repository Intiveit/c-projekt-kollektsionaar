﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H2IProjekt.Domeen
{
    public class EsemeLiik
    {
        public int EsemeLiikId { get; set; }
        public string Nimi { get; set; }
    }
}
