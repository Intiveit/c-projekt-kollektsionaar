﻿using H2IProjekt.Domeen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H2IProjekt.Utilities
{
    // siin klassis funktsionaalsus wishlistis paiknevate teoste otsinguks.
    public class BrauseriAvaja
    {
        private static string _google = "https://www.google.ee/search?&q=";
        private static string _amazon = "https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=";
        private string _otsinguString;
        private string _terveOtsinguString;

        public BrauseriAvaja (Ese valitudEse)
        {
            _otsinguString += valitudEse.Nimi + "+";
            _otsinguString += valitudEse.Autor + "+";
            _otsinguString += valitudEse.Aasta;

            _terveOtsinguString = _amazon + _otsinguString;
        }

        public void GoogeldaEset()
        {
            System.Diagnostics.Process.Start(_terveOtsinguString);
        }
    }
}
