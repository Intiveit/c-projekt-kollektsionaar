﻿using H2IProjekt.Domeen;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H2IProjekt
{
    public class KogudDbContext:DbContext
    {
        public DbSet<Ese> Esemed { get; set; }
        public DbSet<EsemeLiik> EsemeLiigid { get; set; }
        public DbSet<Kasutaja> Kasutajad { get; set; }
        public DbSet<KasutajaKogu> KasutajaKogud { get; set; }
        public DbSet<Kogu> Kogud { get; set; }
        public DbSet<KuulubKogusse> KogusseKuulumised { get; set; }

        public KogudDbContext() : base("name=KogudSql") {  }

        // Kuna EF automaatselt lisab tabelitele inglisekeelse "s" lõpu, siis järgnev meetod eemaldab selle.
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
