﻿using H2IProjekt.Domeen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H2IProjekt.Services.Interfaces
{
    interface IKasutajaService
    {
        void LooKasutaja(Kasutaja uusKasutaja);
        bool KontrolliKasutajat(Kasutaja uusKasutaja);
    }
}
