﻿using H2IProjekt.Domeen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H2IProjekt.Services.Interfaces
{
    public interface IEseService
    {
        Ese LeiaEseIdAbil(int id);

        List<Ese> KoikEsemed();

        Ese LisaEseKogu(Ese ese, int koguId);

        List<Ese> LeiaWishlistisEsemed(Kogu valitudKogu);

        void KustutaEse(Ese ese);

        void KustutaEseKogus(Ese ese);
        List<Ese> LeiaEseStringiAbil(string otsinguString);

        void MuudaEse(Ese valitudEse, Ese uusEse);
    }
}
