﻿using H2IProjekt.Domeen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H2IProjekt.Services.Interfaces
{
    public interface IKoguService
    {
        Kogu LeiaKoguIdAbil(int id);

        List<Kogu> KoikKogud();

        Kogu LisaUusKogu(string nimi);

        void KustutaKogu(int kogu);
        void LisaKasutajaKogu(Kogu kogu, Kasutaja kasutaja);
    }
}
