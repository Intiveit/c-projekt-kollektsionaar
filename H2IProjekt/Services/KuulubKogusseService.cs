﻿using H2IProjekt.Domeen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H2IProjekt.Services
{
    public class KuulubKogusseService:BaasService
    {
        public KuulubKogusseService(KogudDbContext kogudDbContext):base(kogudDbContext)
        {
        }

        public List<KuulubKogusse> KogusseKuulumised(int valitudKoguId)
        {
            return ab.KogusseKuulumised.Where(x => x.KoguId == valitudKoguId).ToList();
        }
    }
}
