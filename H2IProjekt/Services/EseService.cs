﻿using H2IProjekt.Domeen;
using H2IProjekt.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H2IProjekt.Services
{
    public class EseService:BaasService, IEseService
    {
        private KoguService _koguService;
        public EseService(KogudDbContext kogudDbContext):base(kogudDbContext)
        {
            _koguService = new KoguService(kogudDbContext);
        }

        public List<Ese> KoikEsemed()
        {
            return ab.Esemed.ToList();
        }

        public Ese LeiaEseIdAbil(int id)
        {
            return ab.Esemed.Where(x => x.EseId == id).SingleOrDefault();
        }

        public Ese LisaEseKogu(Ese uusEse, int koguId)
        {
            var abEse = ab.Esemed.Where(e => e.EseId == uusEse.EseId).FirstOrDefault();
            var onUus = abEse == null;
            var ese = onUus ? new Ese() : abEse;

            ese.Nimi = uusEse.Nimi;
            ese.Autor = uusEse.Autor;
            ese.Aasta = uusEse.Aasta;
            ese.Hind = uusEse.Hind;
            ese.Kirjeldus = uusEse.Kirjeldus;
            ese.OnOlemas = uusEse.OnOlemas;
            ese.OnWishlistis = uusEse.OnWishlistis;
            ese.Kogus = uusEse.Kogus;

            if (onUus) ab.Esemed.Add(ese);

            var esemeKogu = _koguService.LeiaKoguIdAbil(koguId);

            var kogusseKuulumine = new KuulubKogusse()
            {
                EseId = uusEse.EseId,
                KoguId = esemeKogu.KoguId
            };

            // eseme sidumine koguga:
            ab.KogusseKuulumised.Add(kogusseKuulumine);
            ab.SaveChanges();

            return ese;
        }

        public List<Ese> LeiaWishlistisEsemed(Kogu valitudKogu)
        {
            var esemed = ab.Esemed.Where(x => x.OnWishlistis == true).ToList();
            var valitudKoguID = valitudKogu.KoguId;
            var kogusseKuulumised = ab.KogusseKuulumised.Where(x => x.KoguId == valitudKoguID).ToList();
            var esemedKogusID = kogusseKuulumised.Select(x => x.EseId);

            var wishListisEsemed = new List<Ese>();
            foreach (var eseID in esemedKogusID)
            {
                var tempEse = LeiaEseIdAbil(eseID);
                if (tempEse.OnWishlistis == true) wishListisEsemed.Add(tempEse);
            }

            return wishListisEsemed;
        }

        public void KustutaEse(Ese ese)
        {
            ab.Esemed.Remove(ese);
            KustutaEseKogus(ese);
            ab.SaveChanges();
        }

        public void KustutaEseKogus(Ese ese)
        {
            var kuulumine = ab.KogusseKuulumised.Where(x => x.EseId == ese.EseId).SingleOrDefault();

            ab.KogusseKuulumised.Remove(kuulumine);
            //ab.SaveChanges();
        }

        public List<Ese> LeiaEseStringiAbil(string otsinguString)
        {
            var otinguEsemed = new List<Ese>();
            var pealkirjad = ab.Esemed.Where(x => x.Nimi.ToLower().Contains(otsinguString.ToLower())).ToList();
            var autorid = ab.Esemed.Where(x => x.Autor.ToLower().Contains(otsinguString.ToLower())).ToList();

                foreach (var pealkiri in pealkirjad)
                {
                otinguEsemed.Add(pealkiri);           
                }
            foreach (var autor in autorid)
            {
                otinguEsemed.Add(autor);
            }

            return otinguEsemed;
        }

        public void MuudaEse(Ese valitudEse, Ese uusEse)
        {
            var eseBaasis = ab.Esemed.Where(x => x.EseId == valitudEse.EseId);

            foreach (Ese ese in eseBaasis)
            {
                ese.Nimi = uusEse.Nimi;
                ese.Autor = uusEse.Autor;
                ese.Aasta = uusEse.Aasta;
                ese.Hind = uusEse.Hind;
                ese.OnOlemas = uusEse.OnOlemas;
                ese.OnWishlistis = uusEse.OnWishlistis;
                ese.Kirjeldus = uusEse.Kirjeldus;
                ese.Kogus = uusEse.Kogus;
            }  

            ab.SaveChanges();
        }
    }
}
