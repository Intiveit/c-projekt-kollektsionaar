﻿using H2IProjekt.Domeen;
using H2IProjekt.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H2IProjekt.Services
{
    public class KasutajaKoguService : BaasService, IKasutajaKoguService
    {
        public KasutajaKoguService(KogudDbContext kogudDbContext) : base(kogudDbContext)
        {
        }

        public Kasutaja Valideeri(string kasutaja, string parool)
        {
            var kasutajabaasist = ab.Kasutajad.Where(x => x.Kasutajanimi == kasutaja & x.Parool == parool).SingleOrDefault();
            return kasutajabaasist;
        }

        public List<KasutajaKogu> KogudeKasutajaleKuulumised(int kasutajaId)
        {
            return ab.KasutajaKogud.Where(x => x.KasutajaId == kasutajaId).ToList();
        }
    }
}