﻿using H2IProjekt.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using H2IProjekt.Domeen;

namespace H2IProjekt.Services
{
    public class KasutajaService:BaasService, IKasutajaService
    {
        public KasutajaService(KogudDbContext kogudDbContext):base(kogudDbContext)
        {
        }

        public void LooKasutaja(Kasutaja uusKasutaja)
        {        
            ab.Kasutajad.Add(uusKasutaja);
            ab.SaveChanges();           
         }
        public bool KontrolliKasutajat(Kasutaja uusKasutaja)
        {
        var abKasutaja = ab.Kasutajad.Where(e => e.Kasutajanimi == uusKasutaja.Kasutajanimi).FirstOrDefault();
        return (abKasutaja == null);               
        }
    }
}
