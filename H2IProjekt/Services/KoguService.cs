﻿using H2IProjekt.Domeen;
using H2IProjekt.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H2IProjekt.Services
{
    public class KoguService:BaasService, IKoguService
    {     
        public KoguService(KogudDbContext kogudDbContext):base(kogudDbContext)
        {
        }

        public Kogu LeiaKoguIdAbil(int id)
        {
            return ab.Kogud.Where(x => x.KoguId == id).Single();
        }

        public List<Kogu> KoikKogud()
        {
            return ab.Kogud.ToList();
        }

        public Kogu LisaUusKogu(string nimi)
        {
            var kogu = new Kogu()
            {
                Nimi = nimi
            };

            ab.Kogud.Add(kogu);
            ab.SaveChanges();
            return kogu;
        }

        public void KustutaKogu(int kogu)
        {
            var esemed = ab.KogusseKuulumised.Where(x => x.KoguId == kogu).ToList();

            foreach (var ese in esemed)
            {
                var eseId = ese.EseId;
                var kustuta = ab.Esemed.Where(x => x.EseId == eseId).Single();
                ab.Esemed.Remove(kustuta); 
            }
            // otsib nime järgi baasist ja kustutab
            ab.Kogud.Remove(ab.Kogud.Where(x => x.KoguId == kogu).Single());
            ab.SaveChanges();

            return;
        }

        public void LisaKasutajaKogu(Kogu kogu, Kasutaja kasutaja)
        {
            ab.KasutajaKogud.Add(new KasutajaKogu(){ KoguId = kogu.KoguId, KasutajaId = kasutaja.KasutajaId});
            ab.SaveChanges();
        }
    }
}
 