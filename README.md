# C# projekt

 Antud rakendus aitab kollektsion��ril oma kogu �le �levaadtet omada. Sobib kollektsion��rile, kes kogub n�iteks raamatuid/filme/kunsti.

## Rakenduse peamised funktsioonid: 
* Saab luua kogusid 
* Saab lisada teoseid kogudesse ja neid hiljem muuta 
* Teostele saab lisada lisaks andmetele ka m�rkke kas teos on olemas v�i on soovinimekirjas 
* Soovinimekirjas olevaid teoseid kuvatakse avavaates teosele klikkides avaneb "amazoni" vasted antud teose kohta 
* Teoseid saab ka otsida teose nime v�i autori kaudu 