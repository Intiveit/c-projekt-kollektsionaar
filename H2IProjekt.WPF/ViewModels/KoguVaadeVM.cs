﻿using H2IProjekt.Domeen;
using H2IProjekt.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H2IProjekt.WPF.ViewModels
{
    public class KoguVaadeVM:BaseVM
    {
        private Kogu _valitudKogu;

        protected ObservableCollection<Ese> _esemed;
        public Ese ValitudEse { get; set; }

        public ObservableCollection<Ese> Esemed
        {
            get { return _esemed; }
            protected set
            {
                _esemed = value;
                NotifyPropertyChanged("Esemed");
            }

        }
        public KoguVaadeVM(Kogu valitud) {
            _koguService = new KoguService(new KogudDbContext());
            _eseService = new EseService(new KogudDbContext());
            _kuulubKogusseService = new KuulubKogusseService(new KogudDbContext());
            _valitudKogu = valitud;
            _esemed = K6ikEsemedKogus();
        }

        // Laeb kõik esemed, mis kuuluvad valitud kogusse
        public ObservableCollection<Ese> K6ikEsemedKogus()
        {
            ObservableCollection<Ese> tagastatudEsemed = new ObservableCollection<Ese>();
            var valitudKoguId = _valitudKogu.KoguId;

            var kogusseKuulumised = _kuulubKogusseService.KogusseKuulumised(valitudKoguId);

            Ese eseKogus;

            foreach (var kuulumine in kogusseKuulumised)
            {
                eseKogus = _eseService.LeiaEseIdAbil(kuulumine.EseId);
                tagastatudEsemed.Add(eseKogus);
            }


            return tagastatudEsemed;
        }

        // Lisab uue eseme kogusse
        public void LisaEse(Ese uusEse)
        {
            // string nimi, string autor, int aasta, decimal hind, string kirjeldus, bool onolemas = false
            _eseService.LisaEseKogu(uusEse, _valitudKogu.KoguId);
        }

        public void KustutaEse(Ese ese)
        {
            _eseService.KustutaEse(ese);
        }

        public void KustutaKogu(int valitudKogu)
        {
            _koguService.KustutaKogu(valitudKogu);
        }

        public decimal ArvutaKoguV22rtus()
        {
            decimal summa = 0;
            int kogus = 0;
            foreach (var ese in _esemed)
            {
                kogus = ese.Kogus;
                summa += kogus * ese.Hind;
            }

            return summa;
        }


    }
}
