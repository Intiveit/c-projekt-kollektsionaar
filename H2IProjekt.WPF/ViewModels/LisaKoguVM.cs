﻿using H2IProjekt.Domeen;
using H2IProjekt.Services;
using H2IProjekt.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H2IProjekt.WPF.ViewModels
{  
    public class LisaKoguVM:BaseVM
    {

        public LisaKoguVM()
        {
            base._koguService = new KoguService(new KogudDbContext());
        }

        // Uue kogu lisamine
        public void Lisa(string nimi)
        {
            _koguService.LisaUusKogu(nimi);            
        }

        // Uue kogu lisamine kasutaja koguks
        public void LisaKoosKasutajaga(string nimi, Kasutaja kasutaja)
        {
            var uusKogu = _koguService.LisaUusKogu(nimi);
            _koguService.LisaKasutajaKogu(uusKogu, kasutaja);
        }
    }
}
