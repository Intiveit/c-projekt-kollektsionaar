﻿using H2IProjekt.Domeen;
using H2IProjekt.Services;
using H2IProjekt.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using H2IProjekt.Utilities;

namespace H2IProjekt.WPF.ViewModels
{
    public class MainWindowVM:BaseVM
    {
        protected ObservableCollection<Ese> _esemedWishlistis;
        protected ObservableCollection<Ese> _otsitudEsemed;
        protected Kasutaja _praeguneKasutaja;
        public Kogu ValitudKogu { get; set; }
        public Ese ValitudEse { get; set; }
        public Kasutaja PraeguneKasutaja
        {
            get { return _praeguneKasutaja; }
            set { _praeguneKasutaja = value; }
        }
        public ObservableCollection<Ese> EsemedWishlistis
        {
            get { return _esemedWishlistis; }
            set
            {
                _esemedWishlistis = value;
                NotifyPropertyChanged("EsemedWishlistis");
            }
        }
        public ObservableCollection<Ese> OtsitudEsemed
        {
            get { return _otsitudEsemed; }
            private set
            {
                _otsitudEsemed = value;
                NotifyPropertyChanged("OtsitudEsemed");
            }
        }

        public MainWindowVM()
        {
            base._kogud = new ObservableCollection<Kogu>();
            _koguService = new KoguService(new KogudDbContext());
            _eseService = new EseService(new KogudDbContext());
            _kasutajaKoguService = new KasutajaKoguService(new KogudDbContext());
            _kuulubKogusseService = new KuulubKogusseService(new KogudDbContext());

            base._kogud = LaeKogud();
            _esemedWishlistis = new ObservableCollection<Ese>();
            ValitudKogu = new Kogu();
            ValitudEse = new Ese();
        }

        public MainWindowVM(Kasutaja praeguneKasutaja)
        {
            base._kogud = new ObservableCollection<Kogu>();
            _koguService = new KoguService(new KogudDbContext());
            _eseService = new EseService(new KogudDbContext());
            _kasutajaKoguService = new KasutajaKoguService(new KogudDbContext());
            _kuulubKogusseService = new KuulubKogusseService(new KogudDbContext());

            base._kogud = LaeKogud(praeguneKasutaja);
            _esemedWishlistis = new ObservableCollection<Ese>();
            ValitudKogu = new Kogu();
            ValitudEse = new Ese();
        }
        public ObservableCollection<Ese> LaeWishlistiEsemed()
        {
            var esemedWishlistis = _eseService.LeiaWishlistisEsemed(ValitudKogu);

            ObservableCollection<Ese> esemedWishlistisObservable = new ObservableCollection<Ese>(esemedWishlistis);
            this._esemedWishlistis = esemedWishlistisObservable;

            return esemedWishlistisObservable;
        }

        // Eseme otsimine stringi abil
        public void Otsi(string otsinguString)
        {
            var tulemus = _eseService.LeiaEseStringiAbil(otsinguString);

            ObservableCollection<Ese> otsitudEsemed = new ObservableCollection<Ese>();

            foreach (var kogu in _kogud)
            {
                foreach (var ese in tulemus)
                {
                    var kogusseKuulumised = _kuulubKogusseService.KogusseKuulumised(kogu.KoguId);
                    foreach (var kuulumine in kogusseKuulumised)
                    {
                        if (kuulumine.EseId == ese.EseId) otsitudEsemed.Add(ese);
                    }
                }
            }

            _otsitudEsemed = otsitudEsemed;
        }

        // Brauseri avamine ja guugeldamine
        public void AvaBrauser(Ese valitudEse)
        {
            BrauseriAvaja ba = new BrauseriAvaja(valitudEse);
            ba.GoogeldaEset();
        }
    }
}
