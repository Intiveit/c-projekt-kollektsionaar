﻿using H2IProjekt.Domeen;
using H2IProjekt.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H2IProjekt.WPF.ViewModels
{
    public class LisaTeosVM:BaseVM
    {
        private Kogu _valitudKogu;
        private Ese _valitudEse;

        public Ese ValitudEse
        {
            get { return _valitudEse; }
            private set
            {
                _valitudEse = value;
                NotifyPropertyChanged("ValitudEse");
            }
        }

        public LisaTeosVM(Kogu valitud) {
            _eseService = new EseService(new KogudDbContext());
            _valitudKogu = valitud;
        }

         public LisaTeosVM(Ese valitudEse, Kogu valitudKogu) {
            _eseService = new EseService(new KogudDbContext());
            _valitudEse = valitudEse;
            _valitudKogu = valitudKogu;
        }

        // Meetod nii lisab kui ka muudab olemasolevat eset
        public void Lisa(Ese uusEse)
        {
            if (_valitudEse != null)
            {
                _eseService.MuudaEse(_valitudEse, uusEse);            }
            else
            {
                _eseService.LisaEseKogu(uusEse, _valitudKogu.KoguId);
            }
        }
    }
}
