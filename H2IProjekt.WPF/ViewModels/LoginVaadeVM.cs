﻿using H2IProjekt.Domeen;
using H2IProjekt.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H2IProjekt.WPF.ViewModels
{
    public class LoginVaadeVM:BaseVM
    {
        private string _sisestatudKasutaja; 
        private string _sisestatudParool;

        public LoginVaadeVM()
        {
            base._kasutajaKoguService = new KasutajaKoguService(new KogudDbContext());
        }

        // Kasutaja ja parooli valideerimine vaates
        public Kasutaja AndmedSet (string kasutaja, string parool)
        {
            Kasutaja x = new Kasutaja();
            _sisestatudKasutaja = kasutaja;
            _sisestatudParool = parool;

            x = _kasutajaKoguService.Valideeri(_sisestatudKasutaja, _sisestatudParool);
            return x;
        }

    }
}
