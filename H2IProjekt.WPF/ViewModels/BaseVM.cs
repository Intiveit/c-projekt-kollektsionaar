﻿using H2IProjekt.Domeen;
using H2IProjekt.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using H2IProjekt.Services;

namespace H2IProjekt.WPF.ViewModels
{

    public class BaseVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected IKoguService _koguService;
        protected IEseService _eseService;
        protected KuulubKogusseService _kuulubKogusseService;
        protected KasutajaKoguService _kasutajaKoguService;

        protected ObservableCollection<Kogu> _kogud;
        public ObservableCollection<Kogu> Kogud
        {
            get { return _kogud; }
            set
            {
                _kogud = value;
                NotifyPropertyChanged("Kogud");
            }
        }


        // This method is called by the Set accessor of each property.
        // The CallerMemberName attribute that is applied to the optional propertyName
        // parameter causes the property name of the caller to be substituted as an argument.
        protected void NotifyPropertyChanged(String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        // Lae kõik kogud
        public ObservableCollection<Kogu> LaeKogud()
        {
            var kogud = _koguService.KoikKogud();
            var kogud_obs = new ObservableCollection<Kogu>(kogud);
            
            _kogud = kogud_obs;

            return kogud_obs;
        }

        // Lae kasutajale kuuluvad kogud
        public ObservableCollection<Kogu> LaeKogud(Kasutaja praeguneKasutaja)
        {
            var kogud = _koguService.KoikKogud();
            var kasutajaleKuuluvadKogud = _kasutajaKoguService.KogudeKasutajaleKuulumised(praeguneKasutaja.KasutajaId);

            var kasutajaKogud = new ObservableCollection<Kogu>();

            foreach (var kogu in kogud)
            {
                foreach (var kasutajakogu in kasutajaleKuuluvadKogud)
                {
                    if (kogu.KoguId == kasutajakogu.KoguId) kasutajaKogud.Add(kogu);
                }
                
            }

            return kasutajaKogud;
        }
    }
}
