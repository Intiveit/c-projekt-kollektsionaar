﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using H2IProjekt.Domeen;
using H2IProjekt.Services;

namespace H2IProjekt.WPF.ViewModels
{
    public class RegisteeriVaadeVM
    {
        private KasutajaService _kasutajaService;
        public RegisteeriVaadeVM()
        {
            _kasutajaService = new KasutajaService(new KogudDbContext());
        }
        public void LooKasutaja(Kasutaja uusKasutaja)
        {
            _kasutajaService.LooKasutaja(uusKasutaja);
        }
        public bool KontrolliKasutajat(Kasutaja uusKasutaja)
        {
            return _kasutajaService.KontrolliKasutajat(uusKasutaja);
        }
    }
}
