﻿using H2IProjekt.Domeen;
using H2IProjekt.WPF.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace H2IProjekt.WPF
{
    /// <summary>
    /// Kogu lisamise vaade
    /// </summary>
    public partial class LisaKogu : Window
    {
        private LisaKoguVM _vm;
        private Kasutaja _praeguneKasutaja;
        public LisaKogu()
        {
            InitializeComponent();
        }

        public LisaKogu(Kasutaja praeguneKasutaja)
        {
            _praeguneKasutaja = praeguneKasutaja;
            InitializeComponent();
        }

        // "Lisa" nupule vajutamine
        private void btnLisa(object sender, RoutedEventArgs e)
        {            
            _vm = new LisaKoguVM();
            if (txtKoguNimi.Text == "") MessageBox.Show("Kogul peab olema nimi!", "Viga", MessageBoxButton.OK, MessageBoxImage.Error);
            else
            {
                _vm.LisaKoosKasutajaga(txtKoguNimi.Text, _praeguneKasutaja);
                this.Close();
            } 
        }

        // Kui aken sulgub, avatakse mainwindow
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MainWindow uus = new MainWindow(_praeguneKasutaja);
            uus.Show();
        }
    }
}
