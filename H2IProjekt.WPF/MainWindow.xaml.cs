﻿using H2IProjekt.Domeen;
using H2IProjekt.WPF.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace H2IProjekt.WPF
{
    /// <summary>
    /// Põhiaken - MainWindow
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowVM _vm;
        private Kogu _valitudKogu;
        private Kasutaja _praeguneKasutaja;

        public MainWindow(Kasutaja praeguneKasutaja)

        {
            _praeguneKasutaja = praeguneKasutaja;
            InitializeComponent();           
            // dünaamiline eventhandler
            this.Loaded += MainWindow_Loaded; 
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            _vm = new MainWindowVM(_praeguneKasutaja);
            this.DataContext = _vm;
        }

        // Kogu lisamise nupp
        private void btnLisaKogu_Click(object sender, RoutedEventArgs e)
        {
            if (_praeguneKasutaja != null)
            {
                LisaKogu uus = new LisaKogu(_praeguneKasutaja);
                uus.Show();
                this.Close();
            } else
            {
                LisaKogu uus = new LisaKogu();
                uus.Show();
            }
        }

        /// <summary>
        /// Otsimise nupp
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOtsi_Click(object sender, RoutedEventArgs e)
        {
            var OtsinguString = txtOtsi.Text;
            _vm.Otsi(OtsinguString);
            lboxOtsing.ItemsSource = _vm.OtsitudEsemed;
            lboxOtsing.Items.Refresh();
        }

        // Kui kogu peal klikitakse 2 korda, siis avaneb Koguvaade
        private void txtKogu_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {                
                KoguVaade uus = new KoguVaade(_valitudKogu, _praeguneKasutaja);
                uus.Show();
                this.Close();
                //e.Handled = true; // Ei aktiveeri MainWindow'd uuesti
            }

        }

        // Kui mõne kogu peal klikitakse, siis see kogu pannakse private väljaks
        // ja laetakse wishlistis olevad esemed.
        private void lboxMinuKogud_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lboxMinuKogud.SelectedIndex >= 0)
            {
                _valitudKogu = lboxMinuKogud.SelectedItem as Kogu;
                _vm.ValitudKogu = _valitudKogu;
                _vm.EsemedWishlistis = _vm.LaeWishlistiEsemed();
            }

        }

        // Wishlisti eseme peal klikkamine 2 korda
        private void txtWishlist_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                if (lboxWishlist.SelectedIndex >= 0)
                {
                    _vm.ValitudEse = lboxWishlist.SelectedItem as Ese;
                    // avab brauseri koos otsinguga
                    _vm.AvaBrauser(_vm.ValitudEse);

                }
            }
        }

       // Välja logimine
        private void btnLogOut_Click(object sender, RoutedEventArgs e)
        {
            LoginVaade uus = new LoginVaade();
            uus.Show();
            this.Close();
        }

        // Otsing
        private void txtOtsing_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                if (lboxOtsing.SelectedIndex >= 0)
                {
                    _vm.ValitudEse = lboxOtsing.SelectedItem as Ese;
                    LisaTeos uus = new LisaTeos(_vm.ValitudEse, _valitudKogu, _praeguneKasutaja);
                    uus.Show();
                    this.Close();
                }
            }

        }
    }
}
