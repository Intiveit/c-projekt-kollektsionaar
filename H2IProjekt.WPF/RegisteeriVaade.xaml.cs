﻿using H2IProjekt.Domeen;
using H2IProjekt.WPF.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace H2IProjekt.WPF
{
    /// <summary>
    /// Registreerimise vaade
    /// </summary>
    public partial class RegisteeriVaade : Window
    {
        private RegisteeriVaadeVM _vm;
        public RegisteeriVaade()
        {
            _vm = new RegisteeriVaadeVM();
            InitializeComponent();
        }

        // Uue kasutaja loomine
        private void btnLoo_Click(object sender, RoutedEventArgs e)
        {
            var uusKasutaja = new Kasutaja()
            {
                Kasutajanimi = txtKasutaja.Text,
                Parool = txtParool.Password,
                Eesnimi = txtEesnimi.Text,
                Perenimi = txtPerenimi.Text
            };
            if (uusKasutaja.Kasutajanimi.Length < 4 || uusKasutaja.Parool.Length < 4 || uusKasutaja.Eesnimi == ""
                || uusKasutaja.Perenimi == "")
            {
                MessageBox.Show(" Kõik väljad peavad olema täidetud! \n Kasutajanimi ja parool peavad olema vähemlat 4 tähemärki pikad!",
                    "Viga", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (_vm.KontrolliKasutajat(uusKasutaja) == false)
            {
                MessageBox.Show(" Selline kasutajanimi on juba kasutuses! \n Vali uus kasutajanimi!",
                    "Viga", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                _vm.LooKasutaja(uusKasutaja);
                this.Close();
            }
        }
    }
}
