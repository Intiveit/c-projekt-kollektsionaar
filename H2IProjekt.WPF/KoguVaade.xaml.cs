﻿using H2IProjekt.Domeen;
using H2IProjekt.WPF.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace H2IProjekt.WPF
{
    /// <summary>
    /// Vaade, mis kuvab kogus olevad esemed
    /// </summary>
    public partial class KoguVaade : Window
    {
        private KoguVaadeVM _koguvaadeVm;
        protected Ese _valitudEse;
        protected Kogu _valitudKogu;
        private Kasutaja _praeguneKasutaja;

        public KoguVaade(Kogu valitud, Kasutaja praeguneKasutaja)
        {
            InitializeComponent();
            _koguvaadeVm = new KoguVaadeVM(valitud);
            _valitudKogu = valitud;
            _praeguneKasutaja = praeguneKasutaja;
            // Koguvaate pealkirja muutmine
            labelKoguvaade.Content = _valitudKogu.Nimi;
            // Kogu väärtuse arvutamine
            labelKoguv22rtus.Content = _koguvaadeVm.ArvutaKoguV22rtus();
            // Vaate datacontexti sidumine vaatemudeliga bindingu jaoks.
            this.DataContext = _koguvaadeVm;
        }

        // Lisa teos nupp - avatakse LisaTeos vaade
        private void btnLisaTeos_Click(object sender, RoutedEventArgs e)
        {
            LisaTeos uus = new LisaTeos(_valitudKogu, _praeguneKasutaja);
            uus.Show();
            this.Close();
        }

        // Teose eemaldamise nupp
        private void btnEemaldaTeos_Click(object sender, RoutedEventArgs e)
        {            
            if (lboxMinuEsemed.SelectedIndex >= 0)
            {
                _valitudEse = lboxMinuEsemed.SelectedItem as Ese;

                if (MessageBox.Show("Soovid kindlasti seda eset eemaldada?", "Eseme eemaldamine?",
                MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    _koguvaadeVm.KustutaEse(_valitudEse);
                    MessageBox.Show("Ese eemaldatud.");
                    KoguVaade uus = new KoguVaade(_valitudKogu, _praeguneKasutaja);
                    uus.Show();
                    this.Close();
                }

            } else
            {
                MessageBox.Show("Ese on valimata, ei saa eemaldada.", "valimata", 
                    MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            
        }

        // Kogu eemaldamise nupp
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Soovid kindlasti seda kogu eemaldada?", "Kogu eemaldamine?",
                MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                //Tegevus kui vajutada "Yes" (Kui vajutad "No" siis ei juhtu midagi, msgbox sulgub)
                _koguvaadeVm.KustutaKogu(_valitudKogu.KoguId);
                MainWindow uus = new MainWindow(_praeguneKasutaja);
                uus.Show();
                this.Close();
            }
        }


        // Kui klikatakse kaks korda juba olemasoleval esemel
        private void txtEse_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                LisaTeos uus = new LisaTeos(_valitudEse, _valitudKogu, _praeguneKasutaja);
                uus.Show();
                this.Close();
            }
        }

        private void lboxMinuEsemed_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lboxMinuEsemed.SelectedIndex >= 0)
            {
                _valitudEse = lboxMinuEsemed.SelectedItem as Ese;
                //_koguvaadeVm.ValitudEse = _valitudEse;
            }

        }

        // Vajutades "Tagasi", avatakse uuesti mainwindow
        private void btnTagasi_Click(object sender, RoutedEventArgs e)
        {
            MainWindow uus = new MainWindow(_praeguneKasutaja);
            uus.Show();
            this.Close();
        }
    }
}
