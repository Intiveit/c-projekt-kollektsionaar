﻿using H2IProjekt.Domeen;
using H2IProjekt.WPF.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace H2IProjekt.WPF
{
    /// <summary>
    /// Login Vaate loogika
    /// </summary>
    public partial class LoginVaade : Window
    {
        private LoginVaadeVM _vm;
        public LoginVaade()
        {
            _vm = new LoginVaadeVM();
            InitializeComponent();
        }

        // Login nupu vajutamine
        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            var kasutajanimi = txtKasutaja.Text;
            var parool = txtParool.Password;

            var praeguneKasutaja = new Kasutaja();
            praeguneKasutaja = _vm.AndmedSet(kasutajanimi, parool);
            
            if (praeguneKasutaja != null)
            {
                MainWindow mw = new MainWindow(praeguneKasutaja);
                this.Close();
                mw.Show();   
                e.Handled = true;
            } else
            {
                MessageBox.Show("Vale kasutaja või parool!", "Viga",MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        // Registreerimise nupu vajutamine
        private void btnRegisteeri_Click(object sender, RoutedEventArgs e)
        {
            // Avab registreerimisevaate
            RegisteeriVaade reg = new RegisteeriVaade();
            reg.Show();
        }
    }
}
