﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using H2IProjekt.WPF.ViewModels;
using H2IProjekt.Domeen;

namespace H2IProjekt.WPF
{
    /// <summary>
    ///  Eseme lisamise ja muutmise vaade
    /// </summary>
    public partial class LisaTeos : Window
    {
        private LisaTeosVM _vm;
        private Kogu _valitudKogu;
        private Kasutaja _praeguneKasutaja;

        public LisaTeos(Kogu valitud, Kasutaja praeguneKasutaja)
        {
            _vm = new LisaTeosVM(valitud);
            _valitudKogu = valitud;
            InitializeComponent();
            _praeguneKasutaja = praeguneKasutaja;
        }
        public LisaTeos(Ese valitudEse, Kogu valitudKogu, Kasutaja praeguneKasutaja)
        {
            _vm = new LisaTeosVM(valitudEse, valitudKogu);
            InitializeComponent();
            _valitudKogu = valitudKogu;
            _praeguneKasutaja = praeguneKasutaja;
            this.DataContext = _vm;
            btnLisa.Content = "Muuda";
        }

        private void btnLisa_Click(object sender, RoutedEventArgs e)
        {

            var sisestatudAasta = txtAasta.Text;
            var sisestatudKogus = txtKogus.Text;
            var sisestatudHind = txtHind.Text;

            // valideerimised:
            if (short.TryParse(sisestatudAasta, out short aasta)) { }
            if (short.TryParse(sisestatudKogus, out short kogus)) { }
            if (decimal.TryParse(sisestatudHind, out decimal hind)) { }


            var uusEse = new Ese()
            {
                Nimi = txtNimi.Text,
                Autor = txtAutor.Text,
                Kirjeldus = txtKirjeldus.Text,
                Aasta = aasta,
                Hind = hind,
                OnOlemas = (bool)ckboxOlemas.IsChecked,
                OnWishlistis = (bool)ckboxWish.IsChecked,
                Kogus = kogus
            };
            
            if (uusEse.Nimi == "" || uusEse.Autor == "")
            {
                MessageBox.Show("Nimi ja autor ei tohi olla tühjad!!", "Viga", MessageBoxButton.OK, MessageBoxImage.Warning);
            } else
            {
                _vm.Lisa(uusEse);
                this.Close();
            }        
        }

        // Kui aken sulgub, siis avatakse vastavalt kas koguvaade või mainwindow.
        // See oleneb sellest, kas suletakse peale otsingut või teose lisamist kogusse.
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_valitudKogu != null)
            {
                KoguVaade uus = new KoguVaade(_valitudKogu, _praeguneKasutaja);
                uus.Show();
            } else
            {
                MainWindow uus = new MainWindow(_praeguneKasutaja);
                uus.Show();
            }
        }
    }
}
